package ma.procheck;

import com.google.zxing.NotFoundException;
import ma.procheck.services.QRCodeService;
import ma.procheck.services.QRCodeServiceImpl;
import ma.procheck.services.TIFFService;
import ma.procheck.services.TIFFServiceImp;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

public class MainFrame {
    private JPanel mainPanel;
    // Form Fields
    private JLabel labelInput;
    private JTextField tfInput;
    private JButton btnInput;
    private JLabel errorInput;
    private JLabel labelOutput;
    private JTextField tfOutput;
    private JButton btnOutput;
    private JLabel errorOutput;
    private JButton btnLunch;
    private JProgressBar progressBar;
    private JLabel labelProgress;
    private JLabel labelLoader;

    // Logger
    private Logger logger = Logger.getLogger(QRCodeService.class.getName());

    // Services
    private final QRCodeService qrCodeService;
    private final TIFFService tiffService;

    // Workers
    private TIFFWorker tiffWorker;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Procheck");
        frame.setContentPane(new MainFrame().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public MainFrame(final TIFFService tiffService, final QRCodeService qr){
        this.tiffService = tiffService;
        this.qrCodeService = qr;
    }

    public MainFrame() {

        this.qrCodeService = new QRCodeServiceImpl();
        this.tiffService = new TIFFServiceImp();

        btnInput.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setDialogTitle("Open Input Folder");
                int option = fileChooser.showOpenDialog(null);
                if(option == JFileChooser.APPROVE_OPTION){
                    File folder = fileChooser.getSelectedFile();
                    tfInput.setText(folder.getAbsolutePath());
                    errorInput.setText(" ");
                }else{
                    errorInput.setText(" Open command canceled");
                }
            }
        });

        btnOutput.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setDialogTitle("Open Output Folder");
                int option = fileChooser.showOpenDialog(null);
                if(option == JFileChooser.APPROVE_OPTION){
                    File folder = fileChooser.getSelectedFile();
                    tfOutput.setText(folder.getAbsolutePath());
                    errorOutput.setText(" ");
                }else{
                    errorOutput.setText(" Open command canceled");
                }
            }
        });

        btnLunch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input = tfInput.getText();
                String output = tfOutput.getText();
                if (input.equals("") || output.equals("")){
                    JOptionPane.showOptionDialog(null, "Input or Output field not selected yet.", "Error",JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null,null);
                }else{
                    progressBar.setVisible(true);
                    progressBar.setValue(5);
                    labelLoader.setVisible(true);
                    btnLunch.setVisible(false);
                    (tiffWorker = new TIFFWorker()).execute();
                }
            }
        });
    }

    private ArrayList<String> getDirectoryFiles(String dirPAth){
        try {
            File dir = new File(dirPAth);
            File[] files = dir.listFiles();
            ArrayList<String> list = new ArrayList<>();

            if (files != null && files.length > 0) {
                for (File file : files) {
                    // Check if the file is a directory
                    if (file.isDirectory()) {
                        // We will not print the directory name, just use it as a new
                        // starting point to list files from
                        list.addAll(getDirectoryFiles(file.getAbsolutePath()));
                    } else {
                        // We can use .length() to get the file size
                        list.add(file.getAbsolutePath());
                    }
                }
            }
            return list;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return new ArrayList<>();
    }

    private boolean createTextOutput(String path, String fileName, String txt){
        if (!path.endsWith("/"))
            path+="/";
        try {
            PrintWriter output = new PrintWriter(path+fileName+".txt","UTF-8");
            output.write(txt);
            output.close();
            return true;
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return false;
    }

    private class TIFFWorker extends SwingWorker<String, String>{

        private List<String> globalData;
        private int tiffFilesCount;

        @Override
        protected String doInBackground() throws Exception {
            this.globalData = new ArrayList<>();
            ArrayList<String> list = getDirectoryFiles(tfInput.getText()); // get all files in the input directory
            list.removeIf(path -> !FilenameUtils.getExtension(path).equals("tif")); // remove none TIFF files paths
            process(list);
            return null;
        }

        @Override
        protected void done() {
            JOptionPane.showOptionDialog(null, "Operation Success.", "Success", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
            progressBar.setVisible(false);
            labelLoader.setVisible(false);
            labelProgress.setText("");
            btnLunch.setVisible(true);

            AtomicReference<String> gData = new AtomicReference<String>("");
            gData.getAndUpdate(d->d+"\nInput   : "+tfInput.getText());
            gData.getAndUpdate(d->d+"\nTiff's  : "+tiffFilesCount);
            gData.getAndUpdate(d->d+"\nTretes  : "+globalData.size());
            gData.getAndUpdate(d->d+"\n\n ================================ ");

            for (String data: globalData) {
                gData.getAndUpdate(gd->gd+"\n"+data);
                gData.getAndUpdate(d->d+"\n");
            }

            createTextOutput(tfOutput.getText(),"RAPPORT GENERAL",gData.toString());
        }

        @Override
        protected void process(List<String> chunks) {
            AtomicInteger progress = new AtomicInteger();
            tiffFilesCount = chunks.size();
            chunks.forEach(path->{
                System.out.printf(" \n "+path);
                labelProgress.setText(progress+" / "+chunks.size());
                try {
                    AtomicInteger index = new AtomicInteger(0);
                    ArrayList<String> data = new ArrayList<>();
                    File file = new File(path);
                    Path filePath = file.toPath();
                    tiffService.extractImages(new FileInputStream(file)).forEach(img->{
                        boolean hasQR = false;
                        try{
                            hasQR = qrCodeService.hasQRCode(img);
                            String txt = qrCodeService.decodeQR(img);
                            if(txt!=null)
                            {
                                data.add(txt);
                            }
                            System.out.println("Data  : "+txt);
                        } catch (FileNotFoundException | NotFoundException fileNotFoundException) {
                            if (hasQR){
                                data.add("QR code illisible!");
                                System.out.println("Data  : QR code illisible!");
                            }else {
                                data.add(null);
                                System.out.println("Data  : QR code inexistant!");
                            }
                        }
                        catch (IOException ioException) {
                            System.out.println("File Not Found");
//                            ioException.printStackTrace();
                        }
//                        System.out.println(" ------------------- ");
                        index.getAndIncrement();
                    });

                    String dataHeader = "\n Path  : "+FilenameUtils.getPath(path);
                    dataHeader += "\n Name  : "+FilenameUtils.getBaseName(path);
                    dataHeader += "\n Type  : "+ Files.probeContentType(filePath);
                    dataHeader += "\n Size  : "+ FileUtils.byteCountToDisplaySize(Files.size(filePath));
                    dataHeader += "\n Pages : "+data.size();
                    dataHeader += "\n QR's  : "+data.stream().filter(Objects::nonNull).count();
                    dataHeader += "\n\n ******************* \n";
                    AtomicReference<String> txt = new AtomicReference<>(dataHeader);
                    AtomicInteger pageIndex = new AtomicInteger();
                    data.forEach(page->{
                        txt.getAndUpdate(ref->ref+"\n Index : "+pageIndex);
                        if (page!=null)
                            txt.getAndUpdate(ref->ref+"\n Data  : "+page);
                        else
                            txt.getAndUpdate(ref->ref+"\n Data  : QR code inexistant!");
                        txt.getAndUpdate(ref->ref+"\n\n ------------- \n");
                        pageIndex.getAndIncrement();
                    });
                    createTextOutput(tfOutput.getText(),FilenameUtils.getBaseName(path),txt.toString());
                    this.globalData.add(txt.toString());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                progress.getAndIncrement();
                progressBar.setValue(progress.get()*100/chunks.size());
                labelProgress.setText(progress.get()+" / "+chunks.size());
            });

        }
    }

}
