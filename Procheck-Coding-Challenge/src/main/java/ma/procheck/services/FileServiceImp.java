package ma.procheck.services;

import java.io.File;

public class FileServiceImp implements FileService{
    @Override
    public File getFileByPath(String path) {
        return new File(path);
    }
}
