package ma.procheck.services;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.util.List;

public interface TIFFService {
    public Integer totalPages(File file);
    public List<BufferedImage> extractImages(InputStream fileInput) throws Exception;
}
