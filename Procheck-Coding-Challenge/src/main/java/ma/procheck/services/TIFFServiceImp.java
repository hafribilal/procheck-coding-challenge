package ma.procheck.services;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TIFFServiceImp implements TIFFService {

    private Logger logger = Logger.getLogger(TIFFService.class.getName());

    @Override
    public Integer totalPages(File file) {
        try (ImageInputStream iis = ImageIO.createImageInputStream(file)) {
            ImageReader reader = getTiffImageReader();
            reader.setInput(iis);
            int pages = reader.getNumImages(true);
            return pages;
        } catch (IOException e) {
            logger.log(Level.WARNING,e.getMessage(), e);
            return null;
        }
    }

    @Override
    public List<BufferedImage> extractImages(InputStream fileInput) throws Exception {
        List<BufferedImage> extractedImages = new ArrayList<>();
        try (ImageInputStream iis = ImageIO.createImageInputStream(fileInput)) {
            ImageReader reader = getTiffImageReader();
            reader.setInput(iis);

            int pages = reader.getNumImages(true);
            System.out.printf(" ( PAGES : "+pages+" ) \n");

            for (int imageIndex = 0; imageIndex < pages; imageIndex++) {
                BufferedImage bufferedImage = reader.read(imageIndex);
                extractedImages.add(bufferedImage);
            }
        } catch (IOException e) {
            logger.log(Level.WARNING,e.getMessage(), e);
        }

        return extractedImages;
    }

    private ImageReader getTiffImageReader() {
        Iterator<ImageReader> imageReaders = ImageIO.getImageReadersByFormatName("TIFF");
        if (!imageReaders.hasNext()) {
            throw new UnsupportedOperationException("No TIFF Reader found!");
        }
        return imageReaders.next();
    }
}
