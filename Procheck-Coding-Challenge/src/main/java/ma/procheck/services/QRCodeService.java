package ma.procheck.services;

import com.google.zxing.NotFoundException;

import java.awt.image.BufferedImage;
import java.io.IOException;

public interface QRCodeService {
    String decodeQR(BufferedImage img) throws IOException, NotFoundException;
    Boolean hasQRCode(BufferedImage img) throws NotFoundException;
}
