package ma.procheck.services;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.DetectorResult;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.detector.Detector;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

public class QRCodeServiceImpl implements QRCodeService {

    private Logger logger = Logger.getLogger(QRCodeService.class.getName());

    @Override
    public String decodeQR(BufferedImage img) throws IOException, NotFoundException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(img, "tif", baos);
        byte[] qrCodeBytes = baos.toByteArray();

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(qrCodeBytes);
        BufferedImage bufferedImage = ImageIO.read(byteArrayInputStream);
        BufferedImageLuminanceSource bufferedImageLuminanceSource = new BufferedImageLuminanceSource(bufferedImage);
        HybridBinarizer hybridBinarizer = new HybridBinarizer(bufferedImageLuminanceSource);
        BinaryBitmap binaryBitmap = new BinaryBitmap(hybridBinarizer);
        MultiFormatReader multiFormatReader = new MultiFormatReader();
        Result result = multiFormatReader.decode(binaryBitmap);

        return result.getText();
    }

    // Function to read the QR file
    @Override
    public Boolean hasQRCode(BufferedImage img) {
        try {
            BufferedImageLuminanceSource bils = new BufferedImageLuminanceSource(img); // Here I assumed the BufferdImage object name is "bufferedImage"
            HybridBinarizer hb = new HybridBinarizer(bils);//bils is BufferedImageLuminanceSource object
            BitMatrix bm = hb.getBlackMatrix();
            Detector detector = new Detector(bm);
            DetectorResult result = detector.detect();
            return true;
        } catch (FormatException formatException){
            formatException.printStackTrace();
            return true;
        }catch (NotFoundException notFoundException){
            return false;
        }
    }
}
