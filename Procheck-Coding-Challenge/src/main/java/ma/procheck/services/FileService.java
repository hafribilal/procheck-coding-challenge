package ma.procheck.services;

import java.io.File;

public interface FileService {
    public File getFileByPath(String path);
}
